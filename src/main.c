#include "mem.h"
#include <stdio.h>
#include <stdlib.h>

#define MIN_PAGE_SIZE 4096
#define TESTS_COUNT 4

struct test
{
    size_t initial;
    size_t query_count;
    size_t *queries;
};

enum test_result
{
    TEST_OK = 0,
    TEST_FAIL_INIT,
    TEST_FAIL_ALLOC,
    TEST_FAIL_FREE,
    TEST_FAIL_REALLOC,
    SIMPLE_ALLOC_FAIL_LOL
};
static const char *test_result_strings[] = {
    "Test performed successfully",
    "Test failed on initialization",
    "Test failed on allocation",
    "Test failed on free",
    "Test failed on reallocation",
    "Malloc doesn't work, lol"};

enum test_result test_perform(struct test *test)
{
    void *heap = heap_init(test->initial);
    if (heap == NULL)
        return TEST_FAIL_INIT;
    /*allocate query, free query, one by one*/
    for (size_t i = 0; i < test->query_count; i++)
    {
        void *ptr = _malloc(test->queries[i]);
        if (ptr == NULL)
            return TEST_FAIL_REALLOC;
        /*filling it with some data*/
        for (size_t j = 0; j < test->queries[i]; j++)
        {
            *((char *)ptr + j) = (char)j;
        }
        _free(ptr);
    }
    if (!heap)
    {
        return TEST_FAIL_FREE;
    }
    /*allocate all queries, then free them*/
    void **ptrs = malloc(sizeof(void *) * test->query_count);
    if (ptrs == NULL)
        return SIMPLE_ALLOC_FAIL_LOL;
    printf("Printing last uint64_t number in each allocated block:\n");
    for (size_t i = 0; i < test->query_count; i++)
    {
        ptrs[i] = _malloc(test->queries[i]);
        if (ptrs[i] == NULL)
            return TEST_FAIL_ALLOC;
        /*filling this ptrs[i] with uint64_t (there will be less of them because sizeof(uint64_t) is bigger than sizeof(char)*/
        for (size_t j = 0; j < test->queries[i] / sizeof(uint64_t)-1; j++)
        {
            *((uint64_t *)ptrs[i] + j) = (uint64_t)j;
        }
        /*printing last uint64_t number*/
        printf("%lu\n", *((uint64_t *)ptrs[i] + test->queries[i] / sizeof(uint64_t) - 2));
    }
    for (size_t i = 0; i < test->query_count; i++)
    {
        _free(ptrs[i]);
    }
    free(ptrs);
    if (!heap)
    {
        return TEST_FAIL_FREE;
    }
    heap_term();
    return TEST_OK;
}
int main()
{
    struct test tests[] =
        {
            {MIN_PAGE_SIZE*4, 10, (size_t[]){100,200,300,400,500,600,700,800,900,1000}},
            {MIN_PAGE_SIZE, 10, (size_t[]){400, 400, 400, 400, 400, 400, 400, 400, 400, 400}},
            {MIN_PAGE_SIZE, 10, (size_t[]){410, 410, 410, 410, 410, 410, 410, 410, 410, 410}},
            {MIN_PAGE_SIZE, 7, (size_t[]){MIN_PAGE_SIZE * 4, MIN_PAGE_SIZE + 1, MIN_PAGE_SIZE + 2, MIN_PAGE_SIZE + 3, MIN_PAGE_SIZE + 4, MIN_PAGE_SIZE + 5, MIN_PAGE_SIZE * 10}}};
    for (size_t i = 0; i < TESTS_COUNT; i++)
    {
        enum test_result result = test_perform(&tests[i]);
        printf("Test %lu: %s\n", i + 1, test_result_strings[result]);
    }
    return 0;
}
